﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GeometryApp
{
    public partial class GeometryAppForm : Form
    {
        public GeometryAppForm()
        {
            InitializeComponent();
            
        }

        private void GeometryAppForm_Load(object sender, EventArgs e)
        {
            GeometryWorker geo = new GeometryWorker();
            //it will run through the application and close the windows
            geo.Runner();
            this.Close();
        }
    }
}
