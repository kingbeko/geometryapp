﻿using Newtonsoft.Json;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace GeometryApp
{
    class GeometryWorker
    {
        string connectionString;
        string[] sites;
        string tablename;
        public GeometryWorker()
        {
            connectionString = Properties.Settings.Default.ConnectionString;
            sites = Properties.Settings.Default.SiteList.Split(',');
            tablename = string.Format("\"{0}\"",Properties.Settings.Default.TableName);
        }

        public void Runner()
        {
            foreach (string sit in sites)
            {
                int site = 0;
                Int32.TryParse(sit, out site);

                //int site = 3;
                List<Intersection> intersections = ListIntersections(site);
                //for site wit no intersections
                if (intersections.Count == 0)
                {
                    Logger.WriteLog(site, 0, "No action called ", "No intersections in the site");
                }
                foreach (Intersection intersection in intersections)
                {
                    int action = GeometryReader(site, intersection.intersection_id);
                    switch (action) //more case to come
                    {
                        case 1: //insert new record
                            insertor(site, intersection); break;
                        case 2: //updating an existing record
                            updator(site, intersection); break;
                        default://log the record have been read
                            Logger.WriteLog(site, intersection.intersection_id, "No action called ", "");
                            break;

                    }
                }
            }


        }

        //to insert data in the database
        private void insertor(int site, Intersection intersection)
        {
            //data gathering
            string jsondata = JsonFormater(intersection.id);
            var sql = @"INSERT INTO public." + tablename + @"(""SiteID"",""IntersectionID"",""GeometryJson"",""Status"") VALUES (" + site + " , " + intersection.intersection_id + " , '" + jsondata + "','O')";
            try
            {
                exeSql(sql);
                Logger.WriteLog(site, intersection.intersection_id, "Insertion", "Successfull");
            }
            catch (Exception ex)
            {
                Logger.WriteLog(site, intersection.intersection_id, "Insertion", "  Error : " + ex.Message.ToString());
            }
        }
        
        //to update data in the database
        private void updator(int site, Intersection intersection)
        {
            //data gathering
            string jsondata = JsonFormater(intersection.id);
            //remember to change the status 
            string date = string.Format("{0:yyyy-MM-dd}", DateTime.Now);
            var sql = @"UPDATE public." + tablename + @" set ""GeometryJson"" = '" + jsondata + @"' , ""Status"" = 'O', ""LastUpdate"" ='" + date + @"' where ""SiteID"" = " + site + @" and ""IntersectionID"" = " + intersection.intersection_id;
            try
            {
                exeSql(sql);
                Logger.WriteLog(site, intersection.intersection_id, "Update", "Successfull");
            }
            catch (Exception ex)
            {
                Logger.WriteLog(site, intersection.intersection_id, "Update", "  Error : " + ex.Message.ToString());
            }
        }

        //function to return the lists based on the site:
        private List<Intersection> ListIntersections(int site)
        {
            string sql = @"SELECT intersec.id,intersection_id fROM public.""Intersection"" as intersec join ""Site"" as site on intersec.siteid = site.id where dummy = false and site.site_id =" + site;
            DataSet event_dataset = new DataSet();
            event_dataset = getDataSet(sql);
            List<Intersection> intersections = new List<Intersection>();
            foreach (DataRow da in event_dataset.Tables[0].Rows)
            {
                Intersection intersection = new Intersection();
                intersection.id = Convert.ToInt32(da["id"]);
                intersection.intersection_id = Convert.ToInt32(da["intersection_id"]);
                intersections.Add(intersection);
            }
            return intersections;
        }

        private List<int> ListLinks(int intersection)
        {
            string sql = @"select id from public.""Link"" where  end_intersectionid = " + intersection; //is_mainline = true
            DataSet event_dataset = new DataSet();
            event_dataset = getDataSet(sql);
            List<int> links = new List<int>();
            foreach (DataRow da in event_dataset.Tables[0].Rows)
            {
                links.Add(Convert.ToInt32(da["id"]));
            }
            return links;
        }

        private List<Lane> ListLane(int link)
        {
            string sql = @"select id,phase from public.""Lane"" where linkid = " + link;
            DataSet event_dataset = new DataSet();
            event_dataset = getDataSet(sql);
            List<Lane> lanes = new List<Lane>();
            foreach (DataRow da in event_dataset.Tables[0].Rows)
            {
                Lane Lane = new Lane();
                Lane.lane = Convert.ToInt32(da["id"]);
                Lane.phase = Convert.ToInt32(da["phase"]);
                lanes.Add(Lane);
            }
            return lanes;
        }

        private List<int> ListDetectorsBylane(int lane)
        {
            string sql = @"select detector_id from ""Detector"" as decs join ""DetectorLane"" as declanes on decs.id = declanes.detectorid where laneid = " + lane;
            DataSet event_dataset = new DataSet();
            event_dataset = getDataSet(sql);
            List<int> detectors = new List<int>();
            foreach (DataRow da in event_dataset.Tables[0].Rows)
            {
                detectors.Add(Convert.ToInt32(da["detector_id"]));
            }
            return detectors;
        }

        private List<detector> ListdetectorsbyLink(int link)
        {
            string sql = @"select decs.id,detector_id,is_default_detector,linkid,end_intersectionid,l.start_intersectionid, direction from ""Detector"" as decs join ""Link"" as l on decs.linkid = l.id where linkid = " + link; //is_default_detector = true and
            DataSet event_dataset = new DataSet();
            event_dataset = getDataSet(sql);
            List<detector> detectors = new List<detector>();
            foreach (DataRow da in event_dataset.Tables[0].Rows)
            {
                detector dec = new detector();
                dec.id = Convert.ToInt32(da["id"]);
                dec.detector_id = Convert.ToInt32(da["detector_id"]);
                dec.is_default_detector = (Boolean)da["is_default_detector"];

                Link ln = new Link();
                ln.linkid = Convert.ToInt32(da["linkid"]);
                ln.start_intersectionid = Convert.ToInt32(da["start_intersectionid"]);
                ln.end_intersectionid = Convert.ToInt32(da["end_intersectionid"]);
                ln.direction = da["direction"].ToString();

                dec.link = ln;
                detectors.Add(dec);
            }
            return detectors;
        }

        /* Grid of results
             * Insert in case new = 1
             * Update when status = U ==> 2
             * Do nothing when its O  ==> 0
             * Deleted do nothing its D ==> 0
             * */

        private int GeometryReader(int site, int intersection)
        {
            string sql = @"SELECT ""Status"" FROM public." + tablename + @" where ""SiteID"" =" + site + @" and ""IntersectionID"" = " + intersection;
            DataSet event_dataset = new DataSet();
            event_dataset = getDataSet(sql);

            int result = 1; //default to insert
            
            if (event_dataset.Tables.Count == 0)
            {
                result = 1; // default to insert
            }
            else
            {
                foreach (DataRow da in event_dataset.Tables[0].Rows)
                {
                    var res = da["Status"].ToString();
                    // To decide later on
                    if (res.Equals("U"))
                    { // to update
                        result = 2;
                    }
                    else if (res.Equals("O") || res.Equals("D")) // do nothing
                    {
                        result = 0;
                    }
                    else // to insert
                    {
                        result = 1;
                    }
                }
            }

            return result;
        }

        //query Runner
        private DataSet getDataSet(string sql)
        {
            DataSet myDataSet = new DataSet();
            try
            {
                using (NpgsqlConnection Conn = new NpgsqlConnection(connectionString))
                {
                    Conn.Open();
                    using (NpgsqlDataAdapter myAdapter = new NpgsqlDataAdapter(sql, Conn))
                    {
                        myAdapter.Fill(myDataSet);
                    }
                    Conn.Close(); Conn.Dispose();
                }
            }
            catch (Exception ex)
            {

            }
            return myDataSet;
        }

        public void exeSql(string sql)
        {
            try
            {
                using (NpgsqlConnection Conn = new NpgsqlConnection(connectionString))
                {
                    Conn.Open();
                    using (NpgsqlCommand objCommand = new NpgsqlCommand(sql, Conn))
                    {
                        objCommand.ExecuteNonQuery();
                    }
                    Conn.Close(); Conn.Dispose();
                }
            }
            catch (Exception exp)
            {
                //Conn.Close(); Conn.Dispose();
                throw exp;
            }
        }

        //The JSon formatter
        private string JsonFormater(int intersection)
        {
            string Json = "";
            Geometry geo = new Geometry();
            List<Lane> geoLanes = new List<Lane>();
            List<detector> GeoListdetectorsbyLinks = new List<detector>();
            //get the list of lane and detectors by lane
            List<int> links = ListLinks(intersection);
            foreach (int link in links)
            {
                //get the list of lane and detectors by lane
                List<Lane> lanes = ListLane(link);

                foreach (Lane lane in lanes)
                {
                    lane.detectors = ListDetectorsBylane(lane.lane);
                    geoLanes.Add(lane);
                }
                //get list of detectors by link
                var ListdetectorsbyLinks = ListdetectorsbyLink(link);
                if (ListdetectorsbyLinks.Count != 0)
                {
                    GeoListdetectorsbyLinks.AddRange(ListdetectorsbyLinks);
                }
            }
            geo.Lanes = geoLanes;
            geo.AllDetectorsByIntersection = GeoListdetectorsbyLinks;
            Json = JsonConvert.SerializeObject(geo);

            return Json;
        }
    }

    class Logger
    {
        static string path = Properties.Settings.Default.LogFolder;
        string date = String.Format("{0:g}", DateTime.Now);

        static string file = string.Format("GeometryLog_{0}.txt", string.Format("{0:yyyy-MM-dd h-mm tt}", DateTime.Now));

        public static void WriteLog(int site, int intersection, string operation, string res)
        {
            DateTime date = DateTime.Now;
            var LogFile = string.Format(@"{0}\\{1}", path, file);
            var log = string.Format("{0} : {1} for Site :{2}, Intersection : {3} - {4}",
                date, operation, site, intersection, res
                );

            if (File.Exists(LogFile))
            {
                string logs = File.ReadAllText(LogFile);
                log = logs + Environment.NewLine + log;
            }
            
            File.WriteAllText(LogFile, log);

        }

    }

    public class Geometry
    {
        public List<Lane> Lanes { get; set; }
        public List<detector> AllDetectorsByIntersection { get; set; }
    }
    public class Lane
    {
        public int lane { get; set; }
        public int phase { get; set; }
        public List<int> detectors { get; set; }
    }
    public class detector
    {
        public int id { get; set; }
        public int detector_id { get; set; }
        public bool is_default_detector { get; set; }
        public Link link { get; set; }
    }
    public class Intersection
    {
        public int id { get; set; }
        public int intersection_id { get; set; }
    }
    public class Link
    {
        public int linkid { get; set; }
        public int start_intersectionid { get; set; }
        public int end_intersectionid { get; set; }
        public string direction { get; set; }
    }
}
